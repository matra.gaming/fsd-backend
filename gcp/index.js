const { Datastore } = require("@google-cloud/datastore");
const datastore = new Datastore({
	projectId: 'fsd-challenge',
	keyFilename: 'datastore-credential.json'
});
const moment= require('moment');
const kindName = 'Schedule';

//link: https://us-central1-fsd-challenge.cloudfunctions.net/getSchedules
exports.getSchedules = async (req, res) => {
  
    const query = datastore.createQuery(kindName);

    const [schedule] = await datastore.runQuery(query).catch(err => {
        res.status(500).send(err);
        return;
    });
    res.status(200).send(schedule);
}

//link: https://us-central1-fsd-challenge.cloudfunctions.net/postSchedule
exports.postSchedule = async (req, res) => {
	res.set('Access-Control-Allow-Origin', '*');
    res.set("Access-Control-Allow-Methods", "POST");
    res.set('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    res.set('Access-Control-Allow-Credentials', 'true');
    if (req.method === 'OPTIONS') {
        res.set("Access-Control-Allow-Methods", "POST");
        res.set('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
        res.set('Access-Control-Allow-Credentials', 'true');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
        return;
    }
    if ((req.body.constructor === Object && Object.keys(req.body).length === 0) || req.body.start_date === undefined || req.body.end_date === undefined 
        || !moment(req.body.start_date).isValid() || !moment(req.body.end_date).isValid()) {
        res.status(500).send('The dates in the body may be incorrect!');
        return;
    } 
        const schedule = {
            key: datastore.key(kindName),
            data: {
                start_date: req.body.start_date,
                end_date: req.body.end_date,
            }
        };

        await datastore.save(schedule).then(() => {
            res.status(200).send("Saved new record!")
        }).catch(err => {
            res.status(500).send(err);
        });
}