import { UserController } from "./controller/UserController";
import { ImageController } from "./controller/ImageController";
import { User } from "./entity/User";
import { requiredForAuth } from "./common/middleware";
import { validateUserId, validateUserOnSave, validateUserOnLogin } from "./common/middlewareValidator";

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    middlewares: [requiredForAuth]
},
{
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    middlewares: [requiredForAuth, validateUserOnSave],
},
{
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    middlewares: [requiredForAuth, validateUserId],
},
{
    method: "put",
    route: "/users/:id",
    controller: UserController,
    action: "update",
    middlewares: [requiredForAuth, validateUserId],
},
{
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    middlewares: [requiredForAuth, validateUserId],
},
{
    method: "post",
    route: "/signup",
    controller: UserController,
    action: "signup",
    middlewares: [validateUserOnSave],
},
{
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login",
    middlewares: [validateUserOnLogin],
},
{
    method: "post",
    route: "/evaluate",
    controller: ImageController,
    action: "evaluate"
}];
