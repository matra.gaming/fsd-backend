import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";

export async function validateUserId(request: Request, response: Response, next: NextFunction) {
    let userRepository = getRepository(User);

    if (Number.isNaN(Number(request.params.id))) {
        response.status(400).send("The user id is incorrect!");
        return;
    }
    let foundUser = await userRepository.findOne(request.params.id);
    if (!foundUser) {
        response.status(404).send(`This user id does not exist`);
        return;
    }
    return next();
}

function checkValidUser(request: Request, response: Response, next: NextFunction, usernameValid: boolean) {
    if ((usernameValid && !request.body.userName) || !request.body.email || !request.body.password) {
        response.status(400).send("Please fill in all the required fields.");
        return;
    }
    return next();
}

export function validateUserOnLogin(request: Request, response: Response, next: NextFunction) {
    return checkValidUser(request, response, next, false);
}

export function validateUserOnSave(request: Request, response: Response, next: NextFunction) {
    return checkValidUser(request, response, next, true);
}