import { NextFunction, Request, Response } from "express";

var jwt = require("jsonwebtoken");
require('dotenv').config();

export function requiredForAuth(req: Request, res: Response, next: NextFunction) {
    try {
        let token: String = req.headers.authorization.split(" ")[1];
        if (!jwt.verify(token, process.env.TOKEN_SECRET_KEY)) {
            res.sendStatus(401);
            return;
        }
    } catch (error) {
        console.log("Authorization Header could not be checked!");
        res.sendStatus(401);
        return;
    }
    return next();
}