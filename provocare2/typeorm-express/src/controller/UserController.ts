import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import * as jwt from "jsonwebtoken";
import { MetadataArgsStorage } from "typeorm/metadata-args/MetadataArgsStorage";
require('dotenv').config();
const secretToken = process.env.TOKEN_SECRET_KEY;

export class UserController {

    private userRepository = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
    }
    async removeAll(request: Request, response: Response, next: NextFunction) {
        let users = await this.userRepository.find();
        try {
            await this.userRepository.remove(users);
            return { message: 'succes!' }
        } catch (err) {
            console.log(err)
            return { message: 'fail!' }
        }
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let updatedUser = await this.userRepository.findOne(request.params.id);
        const { userName = updatedUser.userName, email = updatedUser.email, pass = updatedUser.pass } = request.body;

        return await this.userRepository.save({
            id: updatedUser.id, userName, email, pass,
        });
    }

    async login(request: Request, response: Response, next: NextFunction) {
        let user = (
            await this.userRepository.find({
                select: ["userName"],
                where: { email: request.body.email, pass: request.body.pass }
            })
        ).shift();

        if (!user) {
            return { message: "Login failed!" }
        }
        const { email, pass } = request.body;
        return {
            token: jwt.sign({ email, pass }, secretToken, { expiresIn: 5000 }),
            message: "Logged as " + user.userName
        }
    }

    async signup(request: Request, response: Response, next: NextFunction) {
        let user = (
            await this.userRepository.find({
                where: [
                    { userName: request.body.userName },
                    { email: request.body.email },
                ]
            })
        )
        if (user.length > 0) {
            return { message: 'There is already a user registered with same mail or user' }
        }
        const { username, email, pass } = request.body;
        this.userRepository.save(request.body);
        return { message: "Register Succesful !" }
    }
}