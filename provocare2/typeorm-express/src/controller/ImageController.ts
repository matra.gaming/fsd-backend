import { doInitialPrediction } from "../provocare4/script";
import * as tf from "@tensorflow/tfjs-node";

function toArrayFromBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return ab;
}

export class ImageController {
    async evaluate(request, response) {
        if (!request.files) {
            return response.status(400).send("No files were uploaded.");
        }

        const model = await tf.loadLayersModel("file://model/model.json");
        const arrBuffer = toArrayFromBuffer(request.files.image.data);
        console.log(arrBuffer);
        var preds = await doInitialPrediction(model, arrBuffer);

        return response.status(200).send(preds);
    }

}