import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes";
import { User } from "./entity/User";
import * as fileUpload from "express-fileupload";

createConnection().then(async connection => {

    // create express app

    const app = express();
    app.use(fileUpload({
        useTempFiles: true,
        tempFileDir: '/tmp/'
    }));
    app.use(bodyParser.json());

    var router = express.Router();
    router.use("/", (req: Request, res: Response, next: Function) => {
        next();
    });

    // register express routes from defined application routes
    Routes.forEach(route => {
        if (route.middlewares) {
            route.middlewares.forEach((middleware) => {
                (router as any)[route.method](route.route, middleware);
            });
        }
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    app.use("/", router);

    const result = require("dotenv").config();

    if (result.error) {
        throw result.error;
    }

    // start express server
    app.listen(3000);
    console.log("Express server has started on port 3001. Open http://localhost:3001/users to see results");

}).catch(error => console.log(error));
